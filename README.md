modok
=====

Modok takes a C **head**er file (Get it? Head? M.O.D.O.K.? Not my joke please
don't hurt me.) and generates a Javascript object to use with
[`ffi`](https://github.com/node-ffi/node-ffi) for Node.


How to use it.
==============

```bash
$ modok --help
```

Really. It should explain most of it, except for the `mapfile` parameter, which
should be a JSON file like this (these are actually the defaults):

```json
{
    "type_renames": {
        "const char *": "'string'",
        "char *": "'string'"
    },
    "": {
        "MYLIB_ERROR_": "MyLibError"
    }
}
```


So you wrote a Python script to generate Javascript?
====================================================

Yes.
