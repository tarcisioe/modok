'''A C header file to ffi bindings for node translator.

This module is for parsing C headers and generating ffi bindings
for use with node. It generates the function properties and translates
the types as the user demands through a mapping file.
'''
import json
import re
import sys

from collections import ChainMap
from os.path import basename, splitext

from itertools import filterfalse, tee
from typing import (Callable, Dict, List, Iterable, NamedTuple, Tuple, TypeVar,
                    no_type_check)

import carl


ID = r'(?:\b[a-zA-Z_][a-zA-Z1-9_]*\b)'
TYPE = fr'(?:(?:const\s+)?(?:unsigned\s+)?{ID}(?:\s*[*]+)?)'

ARG = fr'void|(?:(?:({TYPE})\s*({ID})(?:,\s*)?))+'
FUNC = (fr'(?:extern)?\s*({TYPE})\s*({ID})\s*'
        fr'\(((?:(?:{TYPE}\s*{ID}(?:,\s*)?))*|void)\);')
DEFINE = fr'^#\s*define\s+({ID})[\s]+([0-9]+)\s*(?://.*)?$'

DEFAULT_MAPPING = {
    'const char *': "'string'",
    'char *': "'string'",
}

FUNCRE = re.compile(FUNC, re.MULTILINE)
ARGRE = re.compile(ARG, re.MULTILINE)
DEFINERE = re.compile(DEFINE, re.MULTILINE)

Mapping = Dict[str, str]
T = TypeVar('T')


class Arg(NamedTuple):
    '''Represent a C function argument'''
    type: str
    name: str


class IntegerConstant(NamedTuple):
    '''Represent a #defined integer constant'''
    name: str
    value: int


class CFunction(NamedTuple):
    '''Represent a C function prototype'''
    return_type: str
    name: str
    args: List[Arg]


class Enum(NamedTuple):
    '''Represent a JS enumeration.'''
    name: str
    values: List[IntegerConstant]


def split_args(arglist: str) -> List[Arg]:
    '''Split a C function prototype's argument list and returns
    a list of Arg.

    Args:
        arglist: a comma separated list of arguments of a C function.

    Returns:
        a list of arguments represented as Arg objects.
    '''
    args = [arg.strip() for arg in arglist.split(',')]
    return [Arg(type, name)
            for arg in args
            for type, name in ARGRE.findall(arg) if arg != 'void']


def read_functions(file_text: str) -> List[CFunction]:
    '''Create a list of CFunction objects from the text of a
    C header file.

    Args:
        file_text: the text of a C header.

    Returns:
        a list of functions representing the API of the header.
    '''
    found = FUNCRE.findall(file_text)
    return [CFunction(return_type, name, split_args(arglist))
            for return_type, name, arglist in found]


def read_constants(file_text: str) -> List[IntegerConstant]:
    '''Create a list of CFunction objects from the text of a
    C header file.

    Args:
        file_text: the text of a C header.

    Returns:
        a list of constants from the header.
    '''
    return [IntegerConstant(name, int(value))
            for name, value in DEFINERE.findall(file_text)]


def rename_type(x: str, mapping: Mapping) -> str:
    '''Remap a type name to something in the mapping, or returns
    it in single quotes, if it is not mapped.

    Args:
        x: the original type name.
        mapping: a mapping of C type names to ffi types.

    Returns:
        str: the type for ffi.
    '''
    return mapping.get(x, f"'{x}'")


def make_binding(function: CFunction, type_renames: Mapping) -> str:
    '''Create a js object property for the library object out of a CFunction
    object.

    Args:
        function: a CFunction object representing a C function prototype.
        type_renames: a mapping from C type names to the type names to use with
                     ffi.

    Returns:
        a JS object property representing a C function.
    '''
    return_type, name, args = function
    argtxt = '\n'.join(f'        {rename_type(type, type_renames)}, // {name}'
                       for type, name in args)
    return (f"    {name}: [{rename_type(return_type, type_renames)}, "
            f"[\n{argtxt}\n    ]]")


def format_library(functions: List[CFunction],
                   type_renames: Mapping,
                   libname: str,
                   libfile: str) -> str:
    '''Create a js object out of a CFunction list.

    Arg:
        functions: a list of CFunction objects.
        type_renames: a mapping from C type names to the type names to use with
                     ffi.
        libname: the name of the library for use as the js object name.
        libfile: the file of the dynamic library to load.

    Returns:
        the formatted JS ffi object.
    '''
    bindings = ',\n'.join(make_binding(f, type_renames) for f in functions)
    return f'''\
export const {libname} = ffi.Library('{libfile}',{{
{bindings}
}});'''


def partition_preds(preds: List[Callable[[T], bool]],
                    iterable: Iterable[T]):
    '''Partition an iterable in n based on n predicates.

    Args:
        preds: a list of predicates to be applied to every element in the
               iterable.
        iterable: the iterable to partition.

    Returns:
        a tuple of iterables with the values that honor the predicates.
    '''
    its = tee(iterable, len(preds))
    return [filter(pred, it) for pred, it in zip(preds, its)]


def partition(pred: Callable[[T], bool],
              iterable: Iterable[T]) -> Tuple[Iterable[T], Iterable[T]]:
    '''Partition an iterable in two based on a predicate.

    Args:
        pred: a predicate to be applyied to every element in the iterable.
        iterable: the iterable to partition.

    Return:
        a tuple of iterables with true and false values, respectively
    '''
    t1, t2 = tee(iterable)
    return filter(pred, t1), filterfalse(pred, t2)


def make_enums(grouped: Iterable[IntegerConstant],
               enums: Mapping) -> List[Enum]:
    '''Does stuff.

    Args:
        grouped: an Iterable of IntegerConstants that belong to enumerations
                 as previously filtered.
        enums: enumerations to make based on constant prefixes.

    Returns
        a list of Enum objects.
    '''
    def make_prefix_checker(prefix):
        '''Closure to check if a string starts with a given prefix.'''
        return lambda x: x.name.startswith(prefix)

    by_prefix = partition_preds([make_prefix_checker(prefix)
                                 for prefix in enums],
                                grouped)

    prefixed = [Enum(enums[prefix], [IntegerConstant(name.replace(prefix, ''),
                                                     value)
                                     for name, value in constants])
                for prefix, constants in zip(enums, by_prefix)]

    return prefixed


def format_enum(enum: Enum):
    '''Format a whole enum into valid Javascript.'''
    values = '\n'.join(f'    {name} = {value},' for name, value in enum.values)
    return f'export enum {enum.name} {{\n{values}\n}};'


def format_constant(constant: IntegerConstant):
    '''Format a single constant into valid Javascript.'''
    return f'export const {constant.name} = {constant.value};'


def format_constants(constants: List[IntegerConstant],
                     enums: Mapping) -> str:
    '''Create js definitions for integer constants.

    Arg:
        constants: a list of IntegerConstant objects.
        enums: enumerations to make based on constant prefixes.

    Returns:
        valid Javascript constant definitions.
    '''
    def has_prefix(c: IntegerConstant):
        '''Closure to check if a string starts with any specified prefix.'''
        return any(c.name.startswith(prefix) for prefix in enums)

    grouped, standalone = partition(has_prefix, constants)
    enum_list = make_enums(grouped, enums)

    formatted_enums = [format_enum(enum) for enum in enum_list]
    formatted_constants = '\n'.join(format_constant(constant)
                                    for constant in standalone)

    return '\n\n'.join((*formatted_enums, formatted_constants))


@no_type_check
@carl.command
def main(
        headerfile: "the C header to translate to ffi.",
        mapfile: "the mapping JSON file to translate type names." = None,
        libname: "the name of the library" = None,
        libfile: "the file of the dynamic library" = None,
):
    '''A ffi binding generator from C headers for node.'''
    if libname is None:
        libname, _ = splitext(basename(headerfile))

    if libfile is None:
        libfile = f'lib{libname}.so'

    with open(headerfile, 'r') as f:
        file_text = f.read()

    mapping = DEFAULT_MAPPING
    configs = {}

    if mapfile is not None:
        with open(mapfile, 'r') as config_file:
            configs = json.load(config_file)

    mapping = ChainMap(configs.get('type_renames', {}), mapping)
    enums = configs.get('enums', {})

    constants = read_constants(file_text)
    functions = read_functions(file_text)

    print(format_constants(constants, enums))
    print()
    print(format_library(functions, mapping, libname, libfile))


def run():
    '''Entry point.'''
    main.run(sys.argv[1:])


if __name__ == '__main__':
    run()
