from setuptools import setup

setup(
    name='modok',
    version='0.0.1',
    py_modules=['modok'],
    entry_points={
        'console_scripts': [
            'modok = modok:run',
        ],
    },
    install_requires=[
        'carl>=0.0.2',
    ],
)
